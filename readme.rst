===============
Public Projects
===============

.. sectnum::
   :suffix: -

**N O T E**

New projects are hosted at `https://src.koda.cnrs.fr/eric.debreuve <https://src.koda.cnrs.fr/eric.debreuve>`_.

The projects still hosted here are listed at `https://gitlab.inria.fr/users/edebreuv/projects <https://gitlab.inria.fr/users/edebreuv/projects>`_.
